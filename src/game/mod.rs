mod session;

use core::iter::once;
use crate::model::GameState;

#[derive(Eq, PartialEq, Debug)]
pub struct RecordedGame {
    states: Vec<GameState>,
}

impl RecordedGame {
    pub fn new() -> Self {
        RecordedGame {
            states: vec![GameState::new()],
        }
    }

    pub fn last(&self) -> &GameState {
        let size = self.states.len();
        &self.states[size - 1]
    }

    pub fn push(self, state: GameState) -> Self {
        Self {
            states: self.states.into_iter().chain(once(state)).collect(),
        }
    }

    pub fn length(&self) -> usize {
        self.states.len()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn t_create() {
        let expected = RecordedGame {
            states: vec![GameState::new()],
        };
        let created = RecordedGame::new();
        assert_eq!(expected, created);
    }

    #[test]
    fn t_last() {
        let record = RecordedGame::new();
        let expected = &record.states[0];
        let state = record.last();
        assert_eq!(expected, state);
    }

    #[test]
    fn t_push() {
        let record = RecordedGame::new();
        let modified = record.push(GameState::new());
        assert_eq!(2, modified.length());
    }
}
