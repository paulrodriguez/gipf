use super::RecordedGame;
use crate::model::Color;
use crate::player::Player;
use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub struct SessionError {
    message: String,
}

impl SessionError {
    pub fn new(message: String) -> Self {
        Self { message }
    }
}

impl Error for SessionError {}

impl fmt::Display for SessionError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

#[derive(PartialEq, Debug)]
struct Session<PW: Player, PB: Player> {
    record: RecordedGame,
    pw: PW,
    pb: PB,
}

impl<PW, PB> Session<PW, PB>
where
    PW: Player,
    PB: Player,
{
    pub fn new(pw: PW, pb: PB) -> Self {
        let record = RecordedGame::new();
        Self { record, pw, pb }
    }

    pub fn turn(&self) -> Color {
        self.record.last().turn()
    }

    pub fn play_turn(self) -> Result<Self, SessionError> {
        let turn = self.turn();
        let player = match self.turn_player() {
            Some(p) => p,
            None => {
                return Err(SessionError::new(
                    "Can't determine next player to take a turn.".to_string(),
                ))
            }
        };
        let choice = match player.play_turn(&self.record) {
            Ok(c) => c,
            Err(e) => {
                let message = format!("player play_turn error: {}", e);
                return Err(SessionError::new(message));
            }
        };

        let gamestate = match player.take_choice(&self.record, choice) {
            Ok(g) => g,
            Err(e) => {
                let message = format!("player take_choice error: {}", e);
                return Err(SessionError::new(message));
            }
        };

        let record = self.record.push(gamestate);
        Ok(Self {
            record,
            pw: self.pw,
            pb: self.pb,
        })
    }

    fn turn_player(&self) -> Option<&dyn Player> {
        match self.turn() {
            Color::White => Some(&self.pw),
            Color::Black => Some(&self.pb),
            Color::Empty => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::player::{MoveAiPlayer, SorterAi};

    #[test]
    fn t_new() {
        let ep1 = MoveAiPlayer::new(SorterAi {});
        let ep2 = MoveAiPlayer::new(SorterAi {});
        let expected = Session {
            record: RecordedGame::new(),
            pw: ep1,
            pb: ep2,
        };
        let pw = MoveAiPlayer::new(SorterAi {});
        let pb = MoveAiPlayer::new(SorterAi {});
        let result = Session::new(pw, pb);
        assert_eq!(expected, result);
        assert_eq!(result.turn(), Color::White);
    }

    #[test]
    fn t_play_turn() {
        let pw = MoveAiPlayer::new(SorterAi {});
        let pb = MoveAiPlayer::new(SorterAi {});
        let start = Session::new(pw, pb);
        let after = start.play_turn();
    }
}
