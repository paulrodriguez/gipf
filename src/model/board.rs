use super::{
    color::Color, coord::Coordinate, line::Line, linespots::LineSpots, mat::Match, mov::Move,
};
use indoc::indoc;
use std::error::Error;
use std::fmt;
use std::str::FromStr;

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Board {
    white: u64,
    black: u64,
}

impl Board {
    pub fn initial_position() -> Self {
        Board::from_str(indoc!(
            "
        W__B
        _____
        ______
        B_____W
         ______
          _____
           W__B"
        ))
        .unwrap()
    }

    pub fn new(white: u64, black: u64) -> Self {
        Board { white, black }
    }

    pub fn count_pieces(&self, color: Color) -> usize {
        let pieces = self.color_pieces(color);
        pieces.count_ones() as usize
    }

    pub fn color(&self, coord: &Coordinate) -> Color {
        let mask = coord.mask();
        if (self.white & mask) != 0 {
            Color::White
        } else if (self.black & mask) != 0 {
            Color::Black
        } else {
            Color::Empty
        }
    }

    pub fn with_color(&self, coord: &Coordinate, color: &Color) -> Board {
        let mask = coord.mask();
        match color {
            Color::White => Board {
                white: self.white | mask,
                black: self.black,
            },
            Color::Black => Board {
                white: self.white,
                black: self.black | mask,
            },
            Color::Empty => {
                let w = self.white & mask;
                let b = self.black & mask;
                Board { white: w, black: b }
            }
        }
    }

    pub fn is_line_full(&self, line: &Line) -> bool {
        let presence = self.white | self.black;
        let mask = line.mask();
        presence & mask == mask
    }

    pub fn line_spots(&self, line: Line) -> LineSpots {
        let spots = line.spots();
        let res: Vec<_> = spots
            .into_iter()
            .map(|s| vec![self.white & s == s, self.black & s == s])
            .enumerate()
            .fold(vec![0u64, 0u64], |a, (i, b)| {
                a.into_iter()
                    .zip(b.into_iter())
                    .map(|(acc, present)| acc + if present { 1 << i } else { 0u64 })
                    .collect()
            });
        LineSpots::new(res[0], res[1], line).unwrap()
    }

    pub fn valid_moves(&self, color: Color) -> Vec<Move> {
        let lines = Line::all_lines().filter(|l| !self.is_line_full(l));
        lines
            .flat_map(|l| {
                vec![true, false]
                    .into_iter()
                    .map(move |d| Move::new(l, color, d).unwrap())
            })
            .collect()
    }

    pub fn find_matches(&self) -> Vec<Match> {
        Line::all_lines()
            .map(|line| self.line_spots(line))
            .flat_map(|ls| Color::bw_iter().map(move |c| (c, ls.line(), ls.match_interval(c))))
            .filter_map(move |(c, line, o)| o.map(|itv| Match::new(line, itv, c)))
            .collect()
    }

    /**
     * Returns the board after the direct effect of a Move.
     *
     * This function only does the initial shift of a move.
     * It does not check for nor implements removing pieces from the board if
     * a match 4 occurs.
     */
    pub fn with_push(&self, mov: &Move) -> Option<Board> {
        if mov.is_valid(self) {
            let line = mov.line();
            let line_spots = self.line_spots(line);
            let color = mov.color();
            let direction = mov.direction();
            let new_spots = line_spots.with_push(color, direction);
            Some(self.with_line(&new_spots))
        } else {
            None
        }
    }

    /**
     * Returns the board with a single changed line.
     */
    pub fn with_line(&self, line_spots: &LineSpots) -> Board {
        let board_lines: Vec<_> = Color::bw_iter()
            .map(|color| {
                line_spots
                    .spots(color)
                    .into_iter()
                    .fold(0, |acc, bits| acc | bits)
            })
            .collect();
        let mask = line_spots.line_mask();
        let w_new = (self.white & !mask) | board_lines[0];
        let b_new = (self.black & !mask) | board_lines[1];
        Board::new(w_new, b_new)
    }

    fn color_pieces(&self, color: Color) -> u64 {
        match color {
            Color::White => self.white,
            Color::Black => self.black,
            Color::Empty => !(self.white | self.black),
        }
    }
}

#[derive(Debug)]
pub struct ParseBoardError {}

impl Error for ParseBoardError {}

impl fmt::Display for ParseBoardError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Encountered unexpected character while parsing board string"
        )
    }
}

impl std::str::FromStr for Board {
    type Err = ParseBoardError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let skip_set = hashset! {'\n', ' '};
        let chars = string.chars().filter(|c| !skip_set.contains(c));
        let flags: Vec<_> = chars
            .zip(Coordinate::board_range())
            .filter_map(|(chr, coord)| {
                let mask = coord.mask();
                match chr {
                    'W' => Some((mask, 0u64)),
                    'B' => Some((0u64, mask)),
                    '_' => Some((0u64, 0u64)),
                    _ => None,
                }
            })
            .collect();
        if flags.len() != Coordinate::total() {
            Err(ParseBoardError {})
        } else {
            let folded = flags
                .iter()
                .fold((0u64, 0u64), |(wa, ba), (w, b)| (wa | w, ba | b));
            let (white, black) = folded;
            let board = Board::new(white, black);
            Ok(board)
        }
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Board({}, {})", self.white, self.black)?;
        for y in 0..7 {
            writeln!(f)?;
            for x in 0..7 {
                let matrixchar = display_helper(self, x, y);
                write!(f, "{}", matrixchar)?;
            }
        }
        writeln!(f)?;
        Ok(())
    }
}

fn display_helper(board: &Board, x: usize, y: usize) -> &str {
    match Coordinate::new(x, y) {
        Some(coord) => match board.color(&coord) {
            Color::White => "W",
            Color::Black => "B",
            Color::Empty => "_",
        },
        None => " ",
    }
}

#[cfg(test)]
mod tests {
    use super::super::linespots::LineInterval;
    use super::*;

    #[test]
    fn with_push_move() {
        let board = Board::new(0, 0);
        let line = Line::from_id(0).unwrap();
        let mov = Move::new(line, Color::White, true).unwrap();
        let new_board = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x1,
            black: 0x0,
        };
        assert_eq!(new_board, expected);
    }

    #[test]
    fn with_push_move_used_board() {
        let board = Board {
            white: 0x1,
            black: 0x0,
        };
        let line = Line::from_id(0).unwrap();
        let mov = Move::new(line, Color::White, true).unwrap();
        let new_board = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x3,
            black: 0x0,
        };
        assert_eq!(new_board, expected);
    }

    #[test]
    fn with_push_move_push() {
        let board = Board {
            white: 0x1,
            black: 0x0,
        };
        let line = Line::from_id(0).unwrap();
        let mov = Move::new(line, Color::Black, true).unwrap();
        let new_board = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x2,
            black: 0x1,
        };
        assert_eq!(expected, new_board);
    }

    #[test]
    fn with_push_move_black() {
        let board = Board::new(0, 0);
        let line = Line::from_id(1).unwrap();
        let mov = Move::new(line, Color::Black, true).unwrap();
        let new_board = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x0,
            black: 0x100,
        };
        assert_eq!(new_board, expected);
    }

    #[test]
    fn with_push_move_reverse() {
        let board = Board::new(0, 0);
        let line = Line::from_id(20).unwrap();
        let mov = Move::new(line, Color::Black, false).unwrap();
        let new_board = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x0,
            black: 0x08000000000000,
        };
        assert_eq!(new_board, expected);
    }

    #[test]
    fn with_push_move_until_full() {
        let line = Line::from_id(17).unwrap();
        let mov = Move::new(line, Color::White, false).unwrap();
        let mut board = Board::new(0, 0);
        let expecteds = vec![
            Some(Board {
                white: 0x40000000000000,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40200000000000,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40201000000000,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40201008000000,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40201008040000,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40201008040200,
                black: 0x0,
            }),
            Some(Board {
                white: 0x40201008040201,
                black: 0x0,
            }),
            None,
        ];
        for expected in expecteds {
            let result = board.with_push(&mov);
            assert_eq!(result, expected);
            board = match result {
                Some(b) => b,
                None => board,
            }
        }
    }

    #[test]
    fn with_push_multiple_moves() {
        let lines = (0..7).map(|i| Line::from_id(i).unwrap());
        let colors = vec![
            Color::White,
            Color::Black,
            Color::White,
            Color::Black,
            Color::White,
            Color::Black,
            Color::White,
        ]
        .into_iter();
        let directions = std::iter::repeat(true)
            .take(3)
            .chain(std::iter::repeat(false).take(3));
        let moves: Vec<_> = lines
            .zip(colors)
            .zip(directions)
            .map(|((l, c), d)| Move::new(l, c, d).unwrap())
            .collect();

        let mut board = Board::new(0, 0);
        for mov in moves {
            board = board.with_push(&mov).unwrap();
        }
        let expected = Board {
            white: 0x004000010001,
            black: 0x400040000100,
        };
        assert_eq!(board, expected);
    }

    #[test]
    fn t_push_line7() {
        let board = Board {
            white: 0x0,
            black: 0x101_0100,
        };
        let mov = Move::new(Line::from_id(7).unwrap(), Color::Black, true).unwrap();
        let result = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x0,
            black: 0x101_0101,
        };
        assert_eq!(expected, result);
    }

    #[test]
    fn t_shift_line20() {
        let board = Board::new(0, 0);
        let mov = Move::new(Line::from_id(20).unwrap(), Color::Black, false).unwrap();
        let result = board.with_push(&mov).unwrap();
        let expected = Board {
            white: 0x0,
            black: 0x8_0000_0000_0000,
        };
        assert_eq!(expected, result);
    }

    #[test]
    fn t_find_matches() {
        let board = Board {
            white: 0xF,
            black: 0x0,
        };
        let matches = board.find_matches();
        let expected = vec![Match::new(
            Line::from_id(0).unwrap(),
            LineInterval::new(0, 4),
            Color::White,
        )];
        assert_eq!(expected, matches);
    }
}
