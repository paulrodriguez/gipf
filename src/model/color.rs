#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
pub enum Color {
    White,
    Black,
    Empty,
}

impl Color {
    pub fn bw_iter() -> impl Iterator<Item = Color> {
        vec![Color::White, Color::Black].into_iter()
    }

    pub fn opponent(&self) -> Self {
        match self {
            Self::White => Self::Black,
            Self::Black => Self::White,
            Self::Empty => Self::Empty,
        }
    }
}
