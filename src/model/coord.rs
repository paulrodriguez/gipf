#[derive(Debug, Eq, PartialEq)]
pub struct Coordinate {
    x: usize,
    y: usize,
}

impl Coordinate {
    pub fn total() -> usize {
        37
    }

    pub fn board_range() -> impl Iterator<Item = Self> {
        (0..7).flat_map(|y| (0..7).filter_map(move |x| Coordinate::new(x, y)))
    }

    pub fn new(x: usize, y: usize) -> Option<Coordinate> {
        let valid = x < 7 && y < 7 && isize::abs(y as isize - x as isize) < 4;
        if valid {
            Some(Coordinate { x, y })
        } else {
            None
        }
    }

    fn offset(&self) -> usize {
        self.y * 8 + self.x
    }

    pub fn mask(&self) -> u64 {
        1u64 << self.offset()
    }
}
