use super::{board::Board, color::Color, line::Line, mat::Match, mov::Move, ParseBoardError};
use std::collections::{HashMap, HashSet};
use std::str::FromStr;
use indoc::indoc;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct StateChoice {
    taker: Color,
    possibilities: HashSet<GameState>,
}

impl StateChoice {
    pub fn no_choice(state: GameState) -> StateChoice {
        StateChoice {
            taker: Color::Empty,
            possibilities: hashset![state],
        }
    }

    pub fn possibilities(&self) -> &HashSet<GameState> {
        &self.possibilities
    }

    pub fn first(self) -> GameState {
        self.possibilities.into_iter().next().unwrap()
    }

    pub fn choose(self, state: &GameState) -> Option<GameState> {
        self.possibilities
            .into_iter()
            .find(|gs| gs == state)
    }
}

#[derive(Debug, Eq, Hash, PartialEq, Clone)]
pub struct GameState {
    board: Board,
    turn: Color,
    w_reserve: usize,
    b_reserve: usize,
}

impl GameState {
    pub fn create_raw(
        board_str: &str,
        turn: Color,
        w_reserve: usize,
        b_reserve: usize,
    ) -> Result<Self, ParseBoardError> {
        Ok(Self {
            board: Board::from_str(board_str)?,
            turn,
            w_reserve,
            b_reserve,
        })
    }

    pub fn new() -> Self {
        GameState {
            board: Board::initial_position(),
            turn: Color::White,
            w_reserve: 15,
            b_reserve: 15,
        }
    }

    pub fn turn(&self) -> Color {
        self.turn
    }

    pub fn win_status(&self) -> Color {
        if self.w_reserve == 0 {
            Color::Black
        } else if self.b_reserve == 0 {
            Color::White
        } else {
            Color::Empty
        }
    }

    pub fn board(&self) -> Board {
        self.board.clone()
    }

    pub fn possible_moves(&self) -> HashSet<Move> {
        let turn = self.turn;
        let all_moves = Line::all_lines().flat_map(|l| {
            vec![true, false]
                .into_iter()
                .flat_map(move |b| Move::new(l, turn, b))
        });
        all_moves
            .filter_map(|m| self.apply_move(&m).map(|_| m))
            .collect()
    }

    pub fn apply_move(&self, mov: &Move) -> Option<StateChoice> {
        if mov.color() != self.turn {
            None
        } else {
            let before_match = self.with_push(mov)?;
            let matches = before_match.board.find_matches();
            let crossings: HashMap<&Match, Vec<_>> = matches
                .iter()
                .map(|m1| {
                    let cross_matches = matches
                        .iter()
                        .filter(|m2| m1.crosses(m2))
                        .collect::<Vec<_>>();
                    (m1, cross_matches)
                })
                .collect();

            let match_refs: Vec<_> = matches.iter().collect();
            if crossings.iter().any(|(_, v)| !v.is_empty()) {
                Some(before_match.resolve_crossing(crossings, &match_refs))
            } else {
                Some(before_match.resolve_nocross(&match_refs))
            }
        }
    }

    fn switch_turn(&self) -> Self {
        GameState {
            board: self.board.clone(),
            turn: self.turn.opponent(),
            w_reserve: self.w_reserve,
            b_reserve: self.b_reserve,
        }
    }

    fn resolve_crossing(
        &self,
        crossings: HashMap<&Match, Vec<&Match>>,
        matches: &[&Match],
    ) -> StateChoice {
        let crosser_matches: Vec<&Match> = crossings
            .into_iter()
            .filter_map(|(m, v)| if v.is_empty() { None } else { Some(m) })
            .collect();
        let crosser_color = crosser_matches[0].color();
        let other_matches: Vec<_> = matches
            .iter()
            .filter(|m| !crosser_matches.contains(m))
            .copied()
            .collect();
        let alternatives = crosser_matches
            .into_iter()
            .map(|m| {
                let state = self.apply_match(m);
                state.state_nocross(&other_matches)
            })
            .collect();
        StateChoice {
            taker: crosser_color,
            possibilities: alternatives,
        }
    }

    fn apply_match(&self, mat: &Match) -> Self {
        let ls = self.board.line_spots(mat.line());
        let joint_interval = ls.interval_extension(mat.interval());
        let cleared_ls = ls.clear_interval(&joint_interval);
        let color = mat.color();
        let (w_reserve, b_reserve) = match color {
            Color::White => {
                let w = self.w_reserve + ls.count_pieces(&joint_interval, color);
                (w, self.b_reserve)
            }
            Color::Black => {
                let b = self.b_reserve + ls.count_pieces(&joint_interval, color);
                (self.w_reserve, b)
            }
            Color::Empty => {
                unreachable!()
            }
        };
        let board = self.board.with_line(&cleared_ls);
        GameState {
            board,
            turn: self.turn,
            w_reserve,
            b_reserve,
        }
    }

    /**
     * Resolves matching lines without same-color crossings.
     *
     * Matches can still cross, but they cannot cross and be of the same color.
     * The pieces are removed from the board with the matches of the given color first and the
     * other color second, if they still apply.
     *
     * Technically, the rules stipulate that the player making the move that causes a mixed
     * color crossing can choose in which order it is resolved.
     * For simplicity and because I can't figure out a scenario in which the player would choose
     * differently, the matches of the turn taker are always resolved first.
     */
    fn resolve_nocross(&self, matches: &[&Match]) -> StateChoice {
        let state = self.state_nocross(matches);
        StateChoice::no_choice(state)
    }

    fn state_nocross(&self, matches: &[&Match]) -> Self {
        let turn_color = self.turn;
        let turn_matches = matches.iter().filter(|m| m.color() == turn_color);
        let noturn_matches = matches.iter().filter(|m| m.color() != turn_color);
        let ordered: Vec<_> = turn_matches.chain(noturn_matches).collect();
        let before_turn = ordered.iter().fold(self.clone(), |state, m| {
            if state.match_is_applicable(m) {
                state.apply_match(m)
            } else {
                state
            }
        });
        before_turn.switch_turn()
    }

    fn match_is_applicable(&self, mat: &Match) -> bool {
        let ls = self.board.line_spots(mat.line());
        ls.has_match(mat.color())
    }

    fn with_push(&self, mov: &Move) -> Option<Self> {
        let board = self.board.with_push(mov)?;
        let (w_reserve, b_reserve) = match mov.color() {
            Color::White => (self.w_reserve - 1, self.b_reserve),
            Color::Black => (self.w_reserve, self.b_reserve - 1),
            _ => return None,
        };
        Some(GameState {
            board,
            turn: self.turn,
            w_reserve,
            b_reserve,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_possible_moves_start() {
        let game = GameState::new();
        let result = game.possible_moves();
        let expected: HashSet<_> = Line::all_lines()
            .flat_map(|l| {
                vec![true, false]
                    .into_iter()
                    .map(move |b| Move::new(l, Color::White, b).unwrap())
            })
            .collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn t_possible_moves_block() {
        let board = Board::from_str(indoc!(
            "
        WWWB
        B___B
        B____B
        B_____W
         W____W
          W___W
           WBBB"
        ))
        .unwrap();
        let game = GameState {
            board,
            turn: Color::White,
            w_reserve: 9,
            b_reserve: 9,
        };
        let result = game.possible_moves();
        let expected: HashSet<_> = [
            "0b+w", "0b-w", "0c+w", "0c-w", "0d+w", "0d-w", "0e+w", "0e-w", "0f+w", "0f-w", "1b+w",
            "1b-w", "1c+w", "1c-w", "1d+w", "1d-w", "1e+w", "1e-w", "1f+w", "1f-w", "2b+w", "2b-w",
            "2c+w", "2c-w", "2d+w", "2d-w", "2e+w", "2e-w", "2f+w", "2f-w",
        ]
        .iter()
        .map(|s| s.parse())
        .flatten()
        .collect();
        assert_eq!(result, expected);
    }

    #[test]
    fn t_game_move() {
        let game = GameState::new();
        let mov = Move::new(Line::from_id(16).unwrap(), Color::White, false).unwrap();
        let result = game.apply_move(&mov).unwrap();
        let exp_board = Board::from_str(indoc!(
            "
        W__B
        _____
        ______
        B_____W
         ______
          ____W
           W__B"
        ))
        .unwrap();
        let expected = StateChoice::no_choice(GameState {
            board: exp_board,
            turn: Color::Black,
            w_reserve: 14,
            b_reserve: 15,
        });
        assert_eq!(expected, result);
    }

    #[test]
    fn t_match_move() {
        let board = Board::new(0x7, 0x0);
        let mov = Move::new(Line::from_id(0).unwrap(), Color::White, true).unwrap();
        let game = GameState {
            board,
            turn: Color::White,
            w_reserve: 12,
            b_reserve: 15,
        };
        let new_state = game.apply_move(&mov);
        let expected = Some(StateChoice::no_choice(GameState {
            board: Board::new(0, 0),
            turn: Color::Black,
            w_reserve: 15,
            b_reserve: 15,
        }));
        assert_eq!(expected, new_state);
    }

    #[test]
    fn t_apply_move_nomatch() {
        let game = GameState {
            board: Board::new(0x2, 0x5),
            turn: Color::Black,
            w_reserve: 14,
            b_reserve: 13,
        };
        let mov = Move::new(Line::from_id(20).unwrap(), Color::Black, false).unwrap();
        let result = game.apply_move(&mov);
        let expected = Some(StateChoice::no_choice(GameState {
            board: Board::new(0x2, 0x8_0000_0000_0005),
            turn: Color::White,
            w_reserve: 14,
            b_reserve: 12,
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn t_apply_move_line7() {
        let board = Board::new(0x0, 0x101_0100);
        let game = GameState {
            board,
            turn: Color::Black,
            w_reserve: 15,
            b_reserve: 12,
        };
        let mov = Move::new(Line::from_id(7).unwrap(), Color::Black, true).unwrap();
        let result = game.apply_move(&mov);
        let expected = Some(StateChoice::no_choice(GameState {
            board: Board::new(0x0, 0x0),
            turn: Color::White,
            w_reserve: 15,
            b_reserve: 15,
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn t_apply_move_crossing() {
        let board = Board::new(0x0, 0x101_010e);
        let game = GameState {
            board,
            turn: Color::Black,
            w_reserve: 15,
            b_reserve: 9,
        };
        let mov = Move::new(Line::from_id(7).unwrap(), Color::Black, true).unwrap();
        let result = game.apply_move(&mov);
        let expected = Some(StateChoice {
            taker: Color::Black,
            possibilities: hashset![
                GameState {
                    board: Board::new(0x0, 0x101_0100),
                    turn: Color::White,
                    w_reserve: 15,
                    b_reserve: 12
                },
                GameState {
                    board: Board::new(0x0, 0xe),
                    turn: Color::White,
                    w_reserve: 15,
                    b_reserve: 12
                },
            ],
        });
        assert_eq!(expected, result);
    }

    #[test]
    fn t_apply_move_crossing2() {
        let board = Board::new(0x20d, 0x20202_0d00);
        let game = GameState {
            board,
            turn: Color::Black,
            w_reserve: 11,
            b_reserve: 9,
        };

        let mov = Move::new(Line::from_id(8).unwrap(), Color::Black, false).unwrap();
        let result = game.apply_move(&mov);
        let expected = Some(StateChoice {
            taker: Color::Black,
            possibilities: hashset![
                GameState {
                    board: Board::new(0xd, 0xd00),
                    turn: Color::White,
                    w_reserve: 11,
                    b_reserve: 12
                },
                GameState {
                    board: Board::new(0x0, 0x202020000),
                    turn: Color::White,
                    w_reserve: 15,
                    b_reserve: 12
                },
            ],
        });
        assert_eq!(expected, result);
    }

    #[test]
    fn t_cross_2colors() {
        let board = Board::new(0x020d, 0x0202020000);
        println!("{}", board);
        let game = GameState {
            board,
            turn: Color::Black,
            w_reserve: 11,
            b_reserve: 12,
        };
        let mov = Move::new(Line::from_id(8).unwrap(), Color::Black, false).unwrap();
        let result = game.apply_move(&mov);
        let expected = Some(StateChoice::no_choice(GameState {
            board: Board::new(0x0d, 0x0),
            turn: Color::White,
            w_reserve: 11,
            b_reserve: 15,
        }));
        assert_eq!(expected, result);
    }

    #[test]
    fn t_win_status() {
        let state = GameState::create_raw(
            indoc!(
                "
                WBWB
                BWBWB
                WBWBW_
                _______
                 WBWBWB
                  WBWBW
                   WBWB"
            ),
            Color::Black,
            0,
            1,
        )
        .unwrap();
        let result = state.win_status();
        assert_eq!(result, Color::Black);
    }
}
