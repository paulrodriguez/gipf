use std::error::Error;
use std::fmt;
use std::ops::Range;
use std::str::FromStr;

#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
pub struct Line {
    id: usize,
}

impl Line {
    pub fn all_lines() -> impl Iterator<Item = Line> {
        (0..21).map(|i| Line::from_id(i).unwrap())
    }

    pub fn from_id(id: usize) -> Option<Line> {
        if id < 21 {
            Some(Line { id })
        } else {
            None
        }
    }

    pub fn new(orientation: Orientation, level: usize) -> Option<Self> {
        if level > 7 {
            return None;
        }
        let orient_offset = (orientation as usize) * 7;
        Some(Line {
            id: level + orient_offset,
        })
    }

    pub fn crosses(&self, other: Line) -> bool {
        let orient_set = hashset![self.orientation(), other.orientation()];
        if orient_set.len() < 2 {
            false
        } else {
            let length_sum = self.length() + other.length();
            if length_sum > 10 {
                false
            } else if orient_set.contains(&Orientation::Horizontal) {
                (self.level() < 3) == (other.level() < 3)
            } else {
                (self.level() < 3) == (other.level() > 3)
            }
        }
    }

    pub fn orientation(&self) -> Orientation {
        if self.id < 7 {
            Orientation::Horizontal
        } else if self.id < 14 {
            Orientation::Vertical
        } else {
            Orientation::Diagonal
        }
    }

    pub fn id(&self) -> usize {
        self.id
    }

    pub fn level(&self) -> usize {
        self.id % 7
    }

    pub fn length(&self) -> usize {
        let level = self.level() as isize;
        let length = 7 - isize::abs(3 - level);
        length as usize
    }

    pub fn mask(&self) -> u64 {
        let spots = self.spots();
        spots.into_iter().fold(0u64, |m, x| m | x)
    }

    pub fn spots(&self) -> Vec<u64> {
        let level = self.level() as isize;
        let width = self.length() as isize;
        let start = isize::max(0, level - 3);
        let end = start + width;
        self.spots_range(start..end)
    }

    pub fn spots_range(&self, range: Range<isize>) -> Vec<u64> {
        let level = self.level() as isize;
        match self.orientation() {
            Orientation::Horizontal => range.map(|i| 1u64 << (level * 8 + i)).collect(),
            Orientation::Vertical => range.map(|i| 1u64 << (i * 8 + level)).collect(),
            Orientation::Diagonal => range.map(|i| 1u64 << (3 - level + i * 9)).collect(),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Hash)]
pub enum Orientation {
    Horizontal,
    Vertical,
    Diagonal,
}

impl Orientation {
    fn from_int(code: usize) -> Option<Self> {
        match code {
            0 => Some(Orientation::Horizontal),
            1 => Some(Orientation::Vertical),
            2 => Some(Orientation::Diagonal),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct ParseLineError {}

impl Error for ParseLineError {}

impl fmt::Display for ParseLineError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Encountered unexpected character while parsing line code"
        )
    }
}

impl std::convert::From<std::num::ParseIntError> for ParseLineError {
    fn from(_int_error: std::num::ParseIntError) -> Self {
        Self {}
    }
}

impl std::str::FromStr for Line {
    type Err = ParseLineError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let orient_code = usize::from_str(&string[0..1])?;
        let orientation = match Orientation::from_int(orient_code) {
            Some(o) => o,
            _ => return Err(ParseLineError {}),
        };
        let level = (string.bytes().collect::<Vec<_>>()[1] - b'a') as usize;
        match Line::new(orientation, level) {
            Some(l) => Ok(l),
            None => Err(ParseLineError {}),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_parse() {
        let line: Line = "0a".parse().unwrap();
        let expected = Line::from_id(0).unwrap();
        assert_eq!(line, expected);
    }

    #[test]
    fn t_from_str() {
        let line = Line::from_str("2d").unwrap();
        let expected = Line::from_id(17).unwrap();
        assert_eq!(line, expected);
    }
}
