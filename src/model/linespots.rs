use super::color::Color;
use super::line::Line;
use std::ops::Range;

#[derive(Debug, Eq, PartialEq)]
pub struct LineSpots {
    white: u64,
    black: u64,
    line: Line,
}

impl LineSpots {
    pub fn new(white: u64, black: u64, line: Line) -> Option<Self> {
        let mask = (1u64 << line.length()) - 1u64;
        let ok = (((white | black) & !mask) == 0u64) && ((white & black) == 0);
        if ok {
            Some(LineSpots { white, black, line })
        } else {
            None
        }
    }

    pub fn line(&self) -> Line {
        self.line
    }

    pub fn line_mask(&self) -> u64 {
        self.line.mask()
    }

    pub fn spots(&self, color: Color) -> Vec<u64> {
        let bits = self.color_spots(color);
        self.line
            .spots()
            .into_iter()
            .enumerate()
            .filter_map(|(i, x)| if bits & (1 << i) == 0 { None } else { Some(x) })
            .collect()
    }

    pub fn count_pieces(&self, interval: &LineInterval, color: Color) -> usize {
        let spots = self.color_spots(color);
        let mask = interval
            .urange()
            .map(|i| 1u64 << i)
            .fold(0u64, |acc, x| acc | x);
        (mask & spots).count_ones() as usize
    }

    /**
     * Returns line spots minus an interval.
     *
     * Removes all pieces in the given interval.
     */
    pub fn clear_interval(&self, interval: &LineInterval) -> LineSpots {
        let mut white = self.white;
        let mut black = self.black;
        for i in interval.urange() {
            let mask = !(1u64 << i);
            white &= mask;
            black &= mask;
        }
        LineSpots {
            white,
            black,
            line: self.line,
        }
    }

    /**
     * Returns an interval from a string of adjacent pieces.
     *
     * Takes an original interval and extends its limits up to the furthest present piece in a row,
     * on both ends.
     */
    pub fn interval_extension(&self, original: &LineInterval) -> LineInterval {
        let spots = !self.color_spots(Color::Empty);

        let mut down = original.start;
        let down_range = (0..(original.start)).rev();
        for i in down_range {
            let mask = 1u64 << i;
            if spots & mask != 0 {
                down = i;
            } else {
                break;
            }
        }

        let mut up = original.end;
        for i in (original.end)..(self.line.length()) {
            let mask = 1u64 << i;
            if spots & mask != 0 {
                up = i + 1;
            } else {
                break;
            }
        }

        LineInterval {
            start: down,
            end: up,
        }
    }

    /**
     * Returns an interval where there are at least 4 pieces in a row of the right color.
     *
     * Returns None if there is no such interval.
     */
    pub fn match_interval(&self, color: Color) -> Option<LineInterval> {
        let spots = self.color_spots(color);

        let mut start = 0;
        let mut end = 0;
        for i in 0..self.line.length() {
            let mask = 1u64 << i;
            if spots & mask == 0 {
                if end - start < 4 {
                    start = i + 1;
                    end = start;
                } else {
                    return Some(LineInterval { start, end });
                }
            } else {
                end = i + 1;
            }
        }

        if end - start >= 4 {
            Some(LineInterval { start, end })
        } else {
            None
        }
    }

    /**
     * Returns whether there is a match of the given color somewhere on the line.
     */
    pub fn has_match(&self, color: Color) -> bool {
        let spots = self.color_spots(color);

        let mut start = 0;
        for i in 0..self.line.length() {
            let mask = 1u64 << i;
            if spots & mask == 0 {
                start = i + 1;
            } else if i - start >= 3 {
                return true;
            }
        }
        false
    }

    /**
     * Count the number of adjacent occupied spots from an extremity.
     */
    pub fn count_adjacent(&self, direction: bool) -> usize {
        let range: Vec<_> = if direction {
            (0..(self.line.length())).collect()
        } else {
            (0..(self.line.length())).rev().collect()
        };
        let spots = !self.color_spots(Color::Empty);
        range
            .into_iter()
            .take_while(|i| {
                let mask = 1u64 << i;
                spots & mask != 0
            })
            .count()
    }

    /**
     * Mask a string of adjacent spots from an extremity.
     */
    fn mask_adjacent(&self, direction: bool) -> u64 {
        let range: Vec<_> = if direction {
            (0..(self.line.length())).collect()
        } else {
            (0..(self.line.length())).rev().collect()
        };
        let spots = !self.color_spots(Color::Empty);
        range
            .into_iter()
            .take_while(|i| {
                let mask = 1u64 << i;
                spots & mask != 0
            })
            .fold(0u64, |acc, i| (acc | (1u64 << i)))
    }

    pub fn with_push(&self, color: Color, direction: bool) -> LineSpots {
        let shift_mask = self.mask_adjacent(direction);
        let colors = vec![self.white, self.black];

        let color_index = match color {
            Color::White => 0,
            Color::Black => 1,
            Color::Empty => panic!(),
        };
        let new_piece = if direction {
            1u64
        } else {
            1u64 << (self.line.length() - 1)
        };
        let new_colors: Vec<_> = colors
            .into_iter()
            .map(|c| {
                let masked = c & shift_mask;
                let shifted = if direction { masked << 1 } else { masked >> 1 };
                ((c & !shift_mask) | shifted) & self.mask()
            })
            .enumerate()
            .map(|(i, c)| if i == color_index { c | new_piece } else { c })
            .collect();
        LineSpots {
            white: new_colors[0],
            black: new_colors[1],
            line: self.line,
        }
    }

    fn with_shift(&self, color: Color, direction: bool) -> LineSpots {
        let l_vec = vec![self.white, self.black];
        let l_mask = self.mask();
        let mut shifted: Vec<_> = l_vec
            .into_iter()
            .map(|x| if direction { x << 1 } else { x >> 1 })
            .map(|x| x & l_mask)
            .collect();

        let color_index = match color {
            Color::White => 0,
            Color::Black => 1,
            Color::Empty => panic!(),
        };
        let added_bit = if direction {
            1u64
        } else {
            1u64 << (self.line.length() - 1)
        };
        shifted[color_index] |= added_bit;
        LineSpots {
            white: shifted[0],
            black: shifted[1],
            line: self.line,
        }
    }

    fn color_spots(&self, color: Color) -> u64 {
        match color {
            Color::White => self.white,
            Color::Black => self.black,
            Color::Empty => !(self.white | self.black),
        }
    }

    fn mask(&self) -> u64 {
        (1u64 << self.line.length()) - 1u64
    }
}

#[derive(Debug, Eq, Hash, PartialEq)]
pub struct LineInterval {
    start: usize,
    end: usize,
}

impl LineInterval {
    pub fn new(start: usize, end: usize) -> LineInterval {
        LineInterval { start, end }
    }

    pub fn length(&self) -> usize {
        self.end - self.start
    }

    pub fn urange(&self) -> Range<usize> {
        (self.start)..(self.end)
    }

    pub fn irange(&self) -> Range<isize> {
        (self.start as isize)..(self.end as isize)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_has_match() {
        let ls = LineSpots {
            white: 0xf,
            black: 0x0,
            line: Line::from_id(0).unwrap(),
        };
        let result = ls.has_match(Color::White);
        assert_eq!(true, result);
    }

    #[test]
    fn t_interval_extension() {
        let spots = LineSpots {
            white: 0x3c,
            black: 0x43,
            line: Line::from_id(3).unwrap(),
        };
        let original = LineInterval::new(2, 6);
        let expected = LineInterval::new(0, 7);
        let result = spots.interval_extension(&original);
        assert_eq!(expected, result);
    }

    #[test]
    fn t_match_interval() {
        let spots = LineSpots {
            white: 0x3c,
            black: 0x43,
            line: Line::from_id(3).unwrap(),
        };
        let expected = Some(LineInterval::new(2, 6));
        let result = spots.match_interval(Color::White);
        assert_eq!(expected, result);
    }

    #[test]
    fn t_match_interval_none() {
        let spots = LineSpots {
            white: 0x71,
            black: 0x0E,
            line: Line::from_id(3).unwrap(),
        };
        let expected = None;
        let result = spots.match_interval(Color::White);
        assert_eq!(expected, result);
    }

    #[test]
    fn t_count_adj() {
        let spots = LineSpots {
            white: 0x1,
            black: 0x2,
            line: Line::from_id(0).unwrap(),
        };
        let result_forward = spots.count_adjacent(true);
        assert_eq!(2, result_forward);
        let result_backward = spots.count_adjacent(false);
        assert_eq!(0, result_backward);
    }
}
