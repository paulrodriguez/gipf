use super::{
    board::Board,
    color::Color,
    line::Line,
    linespots::{LineInterval, LineSpots},
};
use std::collections::HashSet;

/**
 * A 4-in-a-row match of a single color.
 *
 * The interval doesn't contain the extension to adjacent pieces.
 */
#[derive(Debug, Eq, Hash, PartialEq)]
pub struct Match {
    line: Line,
    interval: LineInterval,
    color: Color,
}

impl Match {
    pub fn new(line: Line, interval: LineInterval, color: Color) -> Self {
        Match {
            line,
            interval,
            color,
        }
    }

    pub fn resolved_line(&self, board: &Board) -> LineSpots {
        let original = board.line_spots(self.line);
        let joint_interval = original.interval_extension(&self.interval);
        original.clear_interval(&joint_interval)
    }

    pub fn crosses(&self, other: &Match) -> bool {
        if !self.line.crosses(other.line) || self.color != other.color {
            false
        } else {
            let spots = self.spots();
            let o_spots = other.spots();
            !spots.is_disjoint(&o_spots)
        }
    }

    pub fn color(&self) -> Color {
        self.color
    }

    pub fn line(&self) -> Line {
        self.line
    }

    pub fn interval(&self) -> &LineInterval {
        &self.interval
    }

    fn spots(&self) -> HashSet<u64> {
        let range = self.interval.irange();
        self.line.spots_range(range).into_iter().collect()
    }

    pub fn length(&self) -> usize {
        self.interval.length()
    }
}
