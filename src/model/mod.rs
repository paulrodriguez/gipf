/*!
 * Contains game board logic, computation of possible moves and their effects.
 */

#![allow(dead_code)]

mod board;
mod color;
mod coord;
mod gamestate;
mod line;
mod linespots;
mod mat;
mod mov;

use board::Board;
pub use board::ParseBoardError;
pub use color::Color;
use coord::Coordinate;
pub use gamestate::{GameState, StateChoice};
use indoc::indoc;
use line::Line;
use linespots::LineSpots;
pub use mov::Move;
use std::str::FromStr;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_color() {
        let board = Board::new(0, 0);
        let coord = Coordinate::new(0, 0).unwrap();
        assert_eq!(board.color(&coord), Color::Empty)
    }

    #[test]
    fn test_with_color() {
        let board = Board::new(0, 0);
        let coord = Coordinate::new(0, 1).unwrap();
        let expect = Board::new(0x100, 0);
        assert_eq!(board.with_color(&coord, &Color::White), expect)
    }

    #[test]
    fn display() {
        let coord = Coordinate::new(1, 0).unwrap();
        let board = Board::new(0, 0).with_color(&coord, &Color::White);
        assert_eq!(
            board.to_string(),
            "Board(2, 0)\n_W__   \n_____  \n______ \n_______\n ______\n  _____\n   ____\n"
        )
    }

    #[test]
    fn line_mask() {
        let expected = vec![
            0x0000000000000F,
            0x00000000001F00,
            0x000000003F0000,
            0x0000007F000000,
            0x00007E00000000,
            0x007C0000000000,
            0x78000000000000,
            // Verticals
            0x00000001010101,
            0x00000202020202,
            0x00040404040404,
            0x08080808080808,
            0x10101010101000,
            0x20202020200000,
            0x40404040000000,
            // Diagonals
            0x00000040201008,
            0x00004020100804,
            0x00402010080402,
            0x40201008040201,
            0x20100804020100,
            0x10080402010000,
            0x08040201000000,
        ];
        let lines: Vec<_> = (0..21).map(|i| Line::from_id(i).unwrap().mask()).collect();
        assert_eq!(lines, expected)
    }

    #[test]
    fn line_full() {
        let board = Board::new(0x1010000, 0x0000101);
        let line = Line::from_id(7).unwrap();
        assert!(board.is_line_full(&line))
    }

    #[test]
    fn t_line_spots() {
        let board = Board::new(0x2, 0x200);
        let line = Line::from_id(8).unwrap();
        let expected = LineSpots::new(0x1, 0x2, line).unwrap();
        assert_eq!(board.line_spots(line), expected);
    }

    #[test]
    fn t_board_from_str() {
        let board_str = indoc!(
            "
        BWB_
        _____
        ______
        ______B
         ______
          _____
           W___"
        );
        let board = Board::from_str(board_str).unwrap();
        let expected = Board::new(0x08000000000002, 0x00000040000005);
        assert_eq!(expected, board);
    }

    #[test]
    fn t_coord_range() {
        let result: Vec<_> = Coordinate::board_range().collect();
        let expected = vec![
            Coordinate::new(0, 0).unwrap(),
            Coordinate::new(1, 0).unwrap(),
            Coordinate::new(2, 0).unwrap(),
            Coordinate::new(3, 0).unwrap(),
            Coordinate::new(0, 1).unwrap(),
            Coordinate::new(1, 1).unwrap(),
            Coordinate::new(2, 1).unwrap(),
            Coordinate::new(3, 1).unwrap(),
            Coordinate::new(4, 1).unwrap(),
            Coordinate::new(0, 2).unwrap(),
            Coordinate::new(1, 2).unwrap(),
            Coordinate::new(2, 2).unwrap(),
            Coordinate::new(3, 2).unwrap(),
            Coordinate::new(4, 2).unwrap(),
            Coordinate::new(5, 2).unwrap(),
            Coordinate::new(0, 3).unwrap(),
            Coordinate::new(1, 3).unwrap(),
            Coordinate::new(2, 3).unwrap(),
            Coordinate::new(3, 3).unwrap(),
            Coordinate::new(4, 3).unwrap(),
            Coordinate::new(5, 3).unwrap(),
            Coordinate::new(6, 3).unwrap(),
            Coordinate::new(1, 4).unwrap(),
            Coordinate::new(2, 4).unwrap(),
            Coordinate::new(3, 4).unwrap(),
            Coordinate::new(4, 4).unwrap(),
            Coordinate::new(5, 4).unwrap(),
            Coordinate::new(6, 4).unwrap(),
            Coordinate::new(2, 5).unwrap(),
            Coordinate::new(3, 5).unwrap(),
            Coordinate::new(4, 5).unwrap(),
            Coordinate::new(5, 5).unwrap(),
            Coordinate::new(6, 5).unwrap(),
            Coordinate::new(3, 6).unwrap(),
            Coordinate::new(4, 6).unwrap(),
            Coordinate::new(5, 6).unwrap(),
            Coordinate::new(6, 6).unwrap(),
        ];
        assert_eq!(expected, result);
    }

    #[test]
    fn t_valid_moves() {
        let board = Board::initial_position();
        let w = Color::White;
        let result = board.valid_moves(w);
        let expected: Vec<_> = vec![
            Move::new(Line::from_id(0).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(0).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(1).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(1).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(2).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(2).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(3).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(3).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(4).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(4).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(5).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(5).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(6).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(6).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(7).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(7).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(8).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(8).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(9).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(9).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(10).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(10).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(11).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(11).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(12).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(12).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(13).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(13).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(14).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(14).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(15).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(15).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(16).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(16).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(17).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(17).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(18).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(18).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(19).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(19).unwrap(), w, false).unwrap(),
            Move::new(Line::from_id(20).unwrap(), w, true).unwrap(),
            Move::new(Line::from_id(20).unwrap(), w, false).unwrap(),
        ];
        assert_eq!(expected, result);
    }

    #[test]
    fn t_valid_moves_line0() {
        let board = Board::from_str(indoc!(
            "BBBB
             _____
             ______
             _______
              ______
               _____
                ____"
        ))
        .unwrap();
        let result = board.valid_moves(Color::White);
        let not_expected1 = Move::new(Line::from_id(0).unwrap(), Color::White, false).unwrap();
        let not_expected2 = Move::new(Line::from_id(0).unwrap(), Color::White, true).unwrap();
        let not_expected3 = Move::new(Line::from_id(13).unwrap(), Color::Black, true).unwrap();
        assert!(!result.contains(&not_expected1));
        assert!(!result.contains(&not_expected2));
        assert!(!result.contains(&not_expected3));
    }
}
