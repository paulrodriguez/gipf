use super::{
    color::Color,
    line::{Line, ParseLineError},
    Board,
};
use std::error::Error;
use std::fmt;

#[derive(Debug, Eq, PartialEq, Hash)]
pub struct Move {
    line: Line,
    direction: bool,
    color: Color,
}

impl Move {
    pub fn new(line: Line, color: Color, direction: bool) -> Option<Move> {
        match color {
            Color::Empty => None,
            _ => Some(Move {
                line,
                direction,
                color,
            }),
        }
    }

    pub fn is_valid(&self, board: &Board) -> bool {
        !board.is_line_full(&self.line)
    }

    pub fn line(&self) -> Line {
        self.line
    }

    pub fn direction(&self) -> bool {
        self.direction
    }

    pub fn color(&self) -> Color {
        self.color
    }
}

#[derive(Debug)]
pub struct ParseMoveError {}

impl Error for ParseMoveError {}

impl fmt::Display for ParseMoveError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Encountered unexpected character while parsing move string"
        )
    }
}

impl std::convert::From<ParseLineError> for ParseMoveError {
    fn from(_error: ParseLineError) -> Self {
        Self {}
    }
}

impl std::str::FromStr for Move {
    type Err = ParseMoveError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let line: Line = input[0..2].parse()?;
        let direction = match &input[2..3] {
            "+" => true,
            "-" => false,
            _ => return Err(Self::Err {}),
        };
        let color = match &input[3..4] {
            "w" => Color::White,
            "b" => Color::Black,
            _ => return Err(Self::Err {}),
        };
        match Move::new(line, color, direction) {
            Some(m) => Ok(m),
            None => Err(Self::Err {}),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn t_parse() {
        let mov: Move = "1g+b".parse().unwrap();
        let expected = Move::new("1g".parse().unwrap(), Color::Black, true).unwrap();
        assert_eq!(mov, expected);
    }
}
