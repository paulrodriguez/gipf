mod moveai;

use crate::game::RecordedGame;
use crate::model::{GameState, StateChoice};
use std::error::Error;
use std::fmt;

pub use moveai::{MoveAiPlayer, SorterAi};

#[derive(Debug)]
pub struct PlayerError<'m> {
    message: &'m str,
}

impl<'m> PlayerError<'m> {
    pub fn new(message: &'m str) -> Self {
        Self { message }
    }
}

impl<'m> Error for PlayerError<'m> {}

impl<'m> fmt::Display for PlayerError<'m> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.message)
    }
}

pub trait Player: fmt::Debug {
    fn play_turn(&self, record: &RecordedGame) -> Result<StateChoice, PlayerError>;

    fn take_choice(
        &self,
        record: &RecordedGame,
        state_choice: StateChoice,
    ) -> Result<GameState, PlayerError>;
}
