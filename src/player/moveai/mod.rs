use crate::game::RecordedGame;
use crate::model::{Color, GameState, Move, StateChoice};
use crate::player::{Player, PlayerError};
use indoc::indoc;
use std::collections::HashSet;
use std::fmt;

mod sorter;

pub use sorter::SorterAi;

pub trait MoveAi: fmt::Debug + PartialEq {
    fn preferred_move<'a>(
        &self,
        record: &RecordedGame,
        possibilities: &'a HashSet<Move>,
    ) -> Option<&'a Move>;

    fn resolve_choice(&self, record: &RecordedGame, state_choice: StateChoice) -> GameState;
}

#[derive(Debug, PartialEq)]
pub struct MoveAiPlayer<P>
where
    P: MoveAi,
{
    ai: P,
}

impl<P> MoveAiPlayer<P>
where
    P: MoveAi,
{
    pub fn new(ai: P) -> MoveAiPlayer<P> {
        MoveAiPlayer { ai }
    }
}

impl<P> Player for MoveAiPlayer<P>
where
    P: MoveAi,
{
    fn play_turn(&self, record: &RecordedGame) -> Result<StateChoice, PlayerError> {
        let state = record.last();
        let moves = state.possible_moves();
        let preferred = self.ai.preferred_move(record, &moves);
        match preferred {
            None => Err(PlayerError::new("AI player couldn't choose a move.")),
            Some(mov) => match state.apply_move(mov) {
                None => Err(PlayerError::new("Applying the move failed.")),
                Some(state_choice) => Ok(state_choice),
            },
        }
    }

    fn take_choice<'s>(
        &self,
        record: &RecordedGame,
        state_choice: StateChoice,
    ) -> Result<GameState, PlayerError> {
        Ok(self.ai.resolve_choice(record, state_choice))
    }
}

#[cfg(test)]
mod test {
    use super::sorter::SorterAi;
    use super::*;

    #[test]
    fn t_play_turn() {
        let sorter = SorterAi {};
        let player = MoveAiPlayer { ai: sorter };
        let record = RecordedGame::new();
        let result = player.play_turn(&record).unwrap();
        let expected = StateChoice::no_choice(
            GameState::create_raw(
                indoc!(
                    "
                    W_BW
                    _____
                    ______
                    B_____W
                     ______
                      _____
                       W__B"
                ),
                Color::Black,
                14,
                15,
            )
            .unwrap(),
        );
        assert_eq!(expected, result);
    }

    #[test]
    fn t_take_choice() {
        let sorter = SorterAi {};
        let player = MoveAiPlayer { ai: sorter };
        let state = GameState::create_raw(
            indoc!(
                "
                _WWW
                W____
                W_____
                W______
                 ______
                  _____
                   ____"
            ),
            Color::White,
            9,
            15,
        )
        .unwrap();
        let mov: Move = "0a+w".parse().unwrap();
        let choice = state.apply_move(&mov).unwrap();
        let record = RecordedGame::new().push(state);
        let expected = choice.clone().first();
        let result = player.take_choice(&record, choice).unwrap();
        assert_eq!(expected, result);
    }
}
