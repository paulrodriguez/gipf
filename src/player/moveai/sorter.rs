use super::MoveAi;
use crate::game::RecordedGame;
use crate::model::{Color, GameState, Move, StateChoice};
use std::collections::HashSet;

#[derive(Debug, PartialEq)]
pub struct SorterAi {}

impl MoveAi for SorterAi {
    fn preferred_move<'a>(
        &self,
        _record: &RecordedGame,
        possibilities: &'a HashSet<Move>,
    ) -> Option<&'a Move> {
        possibilities
            .iter()
            .min_by_key(|mov| (mov.line().id(), mov.direction()))
    }

    fn resolve_choice(&self, _record: &RecordedGame, state_choice: StateChoice) -> GameState {
        state_choice.first()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;

    #[test]
    fn t_preferred() {
        let record = RecordedGame::new();
        let game = record.last();
        let moves = game.possible_moves();
        let ai = SorterAi {};
        let result = ai.preferred_move(&record, &moves);
        let expected_move = "0a-w".parse().unwrap();
        let expected: Option<&Move> = Some(&expected_move);
        assert_eq!(result, expected);
    }

    #[test]
    fn t_resolve_choice() {
        let game = GameState::create_raw(
            indoc!(
                "
                ____
                _____
                ______
                ___B___
                 __B___
                  _B___
                   _BBB"
            ),
            Color::Black,
            9,
            9,
        )
        .unwrap();
        let mov: Move = "2g-b".parse().unwrap();
        let state_choice = game.apply_move(&mov).unwrap();
        let record = RecordedGame::new().push(game);

        let expected = state_choice.clone().first();
        let ai = SorterAi {};
        let result = ai.resolve_choice(&record, state_choice);
        assert_eq!(expected, result);
    }
}
